(function () {

var MAX_TIMELINE_CLIPS =54,
    SEQUENCE_NUM = 9,
    day = 0;

$("#shelf,#timeline").sortable({
    connectWith: ".sortable",
    placeholder: "placeholder"
});

function deal () {
    var day_title = $("#shelf li#text"+day);
    if (++day >= 7) { day= 0; }
    // console.log("day_title", day_title);
    $("#timeline").append(day_title);
    var count = $("#timeline li").size();
    for (var i=0; i<SEQUENCE_NUM-count; i++) {
        var shelf = $("#shelf li.clip"),
            random = shelf.eq(Math.floor(Math.random()*shelf.length));
        // console.log("place", random);
        $("#timeline").append(random);
    }    
}

function empty () {
    $("#timeline li").appendTo("#shelf");    
}

function play (elt) {
    if (elt) {
        $(elt).click();
        $("#play").addClass("playing");
    } else {
        if ($("#timeline li").size() > 0) {
            $("#timeline li:first").click();
            $("#play").addClass("playing");
        }
    }
}

function pause() {
    $("#play").removeClass("playing");
    video.pause();
    interval.clear();
}

// $("#random").click(deal);
$("#play").on("click", e=> {
    if ($("#play").hasClass("playing")) {
        pause();
    } else {
        $("#play").addClass("playing");
        if ($("#random").prop("checked") && $("#timeline li").size() == 0) {
            console.log("initial deal");
            deal();
            play();
        } else {
            play();
        }
    }
});
$("#empty").click(empty);

// $("#shelf").on("sortstart", function () {
//     console.log("sortstart");
// }).on("sortstop", function () {
//     console.log("sortstop");
//     // $("#timeline").droppable("enable");
// });

function update_timeline () {
    var timeline = $("#timeline"),
        count = $("li", timeline).size();

    if (count >= MAX_TIMELINE_CLIPS) {
        console.log("timeline full");
        $( "#shelf" ).sortable( "option", "connectWith", "" );
    } else {
        $( "#shelf" ).sortable( "option", "connectWith", ".sortable" );        
    }
}

$("#timeline")
    .on("sortremove", update_timeline)
    .on("sortreceive", update_timeline);

var shelf = document.getElementById("shelf"),
    video = document.getElementById("video"),
    text = document.getElementById("text"),
    text_content = document.getElementById("text-content"),
    text_content_p = text_content.querySelector("p"),
    timeline = document.getElementById("timeline-div"),
    THUMB_WIDTH = 96,  // 80x45,
    THUMB_HEIGHT = 54;

async function init () {
    await init_video();
}


async function init_text (data) {
    if (!data.text) return;
    // console.log("init_text", data.text.length);
    for (var i=0, len=data.text.length; i<len; i++) {
        let t = data.text[i];
        let p = document.createElement("p");
        let link = document.createElement("li");
        if (t.id) { link.id = t.id };
        link.classList.add("text");
        p.innerHTML = t.text;
        link.appendChild(p);
        shelf.appendChild(link);
        link.addEventListener("click", async function (e) {
            e.preventDefault();
            text_content_p.innerHTML = t.text;
            text.classList.add("visible");
            // await wait(10);
            // console.log("calling fitText");
            $(text_content_p).fitText();
            // $(window).trigger("resize");
            var finished = await wait(3000);
            if (!finished) {
                console.log("text cancelled");
                return;
            }
            // text.classList.remove("visible");
            // text_content_p.innerHTML = "";

            if (link.parentNode.parentNode == timeline) {
                if (link.nextElementSibling && $("#play").hasClass("playing")) {
                    link.nextElementSibling.click();
                }
            } 
        })
    }
}

async function preplay (link) {
    if (link.classList.contains("clip")) {
        var start = parseFloat(link.getAttribute("data-start"));
        console.log("preplay", link, start);
        await cue_video(video, start);        
    }
}

async function init_video () {
    var resp = await fetch("everyday.json"),
        data = await resp.json(),
        canvas = document.createElement("canvas"),
        ctx;
    canvas.width = THUMB_WIDTH;
    canvas.height = THUMB_HEIGHT;
    ctx = canvas.getContext("2d");
    ctx.imageSmoothingEnabled = true;
    ctx.imageSmoothingQuality = 'high';
    // console.log("data", data);
    var LIMIT = 0;
    for (var i=0, len=data.clips.length; i<len; i++) {
        let d = data.clips[i];
        // console.log("d", d);
        if ((d.start > video.duration) || (LIMIT && i>=LIMIT)) {
            console.log("reached end of clip, skipping remaining entries");
            break;
        }
        let link = document.createElement("li");
        link.classList.add("clip");
        link.setAttribute("data-start", d.start);
        link.setAttribute("data-duration", d.duration);
        // link.innerHTML = i + " ";
        await cue_video(video, d.start);
        // console.log("d", d, i);
        ctx.drawImage(video, 0, 0, THUMB_WIDTH, THUMB_HEIGHT);
        var img_url = canvas.toDataURL();
        let img = document.createElement("img");
        link.appendChild(img);
        img.src = img_url;       
        link.addEventListener("click", async function (e) {
            // console.log("link click");
            e.preventDefault();
            // console.log("d", d);
            var finished = await play_clip(video, d.start, d.duration);
            if (!finished) {
                console.log("play_clip canceled");
                return;
            }
            // play next clip if in timeline
            // console.log("post play");
            if (link.parentNode.parentNode == timeline) {
                // console.log("PLAY NEXT", link.nextElementSibling);
                if ($("#play").hasClass("playing")) {                
                    if (link.nextElementSibling) {
                        link.nextElementSibling.click();
                    } else {
                        console.log("end of sequence");
                        // if random mode, empty and redeal
                        if ($("#random").prop("checked")) {
                            empty();
                            deal();
                            play();
                        } else {
                            pause();
                        }
                    }
                }
            } 
            // video.play();
        })
        shelf.appendChild(link);
    }
    init_text(data);
}

if (video.readyState >= 1) {
    window.setTimeout(init, 250);    
} else {
    video.addEventListener("durationchanged", e => {  });
}
// document.addEventListener("DOMContentLoaded", init);

function cue_video (video, time) {
    return new Promise(resolve => {
        var canplay = e => {
            video.removeEventListener("canplay", canplay);
            resolve();
        };
        video.addEventListener("canplay", canplay);
        video.currentTime = time;
        if (video.canplay) { canplay(); }
    })
}

function interval_singleton () {
    // ensure a single interval callback function
    // to use:
    // call set with a callback function and timing
    // call clear to clear the callback
    // to use the cancel_callback
    // provide cancel_callback when calling set, and remember the returned interval_id
    // when calling clear in normal case (no cancel), use this interval_id
    var interval_id,
        interval_cancel_callback = null,
        set = function (fn, timing, cancel_callback) {
            clear();
            interval_id = window.setInterval(fn, timing);
            interval_cancel_callback = cancel_callback;
            return interval_id;
        },
        clear = function (from_id) {
            // from_id is used to track if clear is from the same caller that 
            // called set (in which case it's not a CANCEL)
            // or not (in which case cancel callback will get called if provided)
            if (interval_id) {
                // HERE ALSO CALL EVT RESOLVE HOOK FOR PREVIOUS SET
                window.clearInterval(interval_id);
                if (interval_cancel_callback && (from_id !== interval_id)) {
                    interval_cancel_callback.call();
                    interval_cancel_callback = null;
                }
                interval_id = null;
            }
        };
    return {
        'set': set,
        'clear': clear
    };
}

var interval = interval_singleton();

function play_clip (video, start, dur) {
    var frame_rate = 25,
        last_frame_time = start + dur - (4/frame_rate);
    return new Promise(resolve => {
        cue_video(video, start).then(result => {
            // hide any text display NOW...
            text.classList.remove("visible");
            var interval_id,
                intv = e => {
                    // console.log("i", video.currentTime, start+dur);
                    if (video.currentTime >= last_frame_time) {
                        interval.clear(interval_id);
                        video.pause();
                        resolve(true);
                    }
                };
            video.play();
            interval_id = interval.set(intv, 1000/(frame_rate*3), cancel => {
                resolve(false);
            });
        });
    });
}

// need cancelable wait ...
// ability for a STOP to make a wait resolve immediately...
// AND return some kind of true or error...
// use the interval singleton ...
// similarly wait for some final time to arrive
// but watch for the playing state to change...

/*
function wait (time) {
    return new Promise(resolve=> {
        window.setTimeout(function () {
            resolve();
        }, time);
    })
}
*/
function wait (time) {
    return new Promise(resolve=> {
        var startTime = new Date(),
            interval_id = intv = e => {
                // console.log("i", video.currentTime, start+dur);
                if (new Date() - startTime > time) {
                    interval.clear(interval_id);
                    resolve(true);
                }
            };
        interval_id = interval.set(intv, 250, cancel => {
            resolve(false);
        });
    })
}


})();