all: everyday.json

index.html: index.md
	pandoc --from markdown --to html5 --standalone --css style.css $< -o $@

%.json: %.kdenlive
	python3 scripts/kdenlive_to_playlist.py $< > $@