from xml.etree import ElementTree as ET 
import argparse, json, sys


def frames_to_time (frames, framerate):
    return frames / framerate

ap = argparse.ArgumentParser("")
ap.add_argument("input")
ap.add_argument("--startframes", type=int, default=2)
ap.add_argument("--framerate", type=float, default=25.0)
ap.add_argument("--playlist", default="Video 1", help="Track name to render")
ap.add_argument("--output", type=argparse.FileType("w"), default=sys.stdout)
args = ap.parse_args()
t = ET.parse(args.input)

main = t.find('.//playlist[@id="main bin"]')
found = False
for playlist in t.findall(".//playlist"):
    prop = playlist.find(".//property[@name='kdenlive:track_name']")
    if prop is not None:
        if prop.text == args.playlist:
            found = True
            main = playlist
            break
if not found:
    print ("Track named {} not found.".format(args.playlist), file=sys.stderr)
    sys.exit(0)

out= {}
out['clips'] = clips = []

# cur_frames = 1
cur_frames = args.startframes ## weirdly seems to need to be 1 or 2 sometimes, not 0 
for elt in main:
    if elt.tag == "entry":
        producer, fin, fout = elt.attrib["producer"], int(elt.attrib["in"]), int(elt.attrib["out"])
        eframes = fout - fin + 1
        # eframes =  eout - ein + 1
        clips.append({'type': 'entry', 'start_frames': cur_frames, 'start': frames_to_time(cur_frames, args.framerate), 'duration_frames': eframes, 'duration': frames_to_time(eframes, args.framerate)})
        print ("entry", eframes, file=sys.stderr)
        cur_frames += eframes
    elif elt.tag == "blank":
        eframes = int(elt.attrib['length']) + 1
        print ("blank", eframes, file=sys.stderr)
        cur_frames += eframes

    # print (producer, fin, fout)
print (json.dumps(out, indent=2), file=args.output)

